function setupOpenDataPlatformWidget(containerElementId, remote) {
    var containerElement = document.getElementById(containerElementId);

    var rpc = new easyXDM.Rpc(
        {
            remote: remote,
            container: containerElement,
            props: {
                width: "100%"
            }
        },
        {
            local: {
                navigateTo: function (url, successFn, errorFn) {
                    window.location = url;
                },
                setHeight: function (height, successFn, errorFn) {
                    var iframe = containerElement.getElementsByTagName("iframe")[0];
                    iframe.height = height;
                }
            }
        }
    );
}
