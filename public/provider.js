// Based on code from https://raw.githubusercontent.com/guardian/iframe-messenger
// but using easyXDM instead of `window.parent.postMessage` directly.

var REFRESH_DELAY = 200;

function setupPage() {
    // Fix Chrome's scrollbar
    document.documentElement.style.overflow = 'hidden';
}

function computeDocumentHeight() {
    var body = document.body,
        html = document.documentElement;

    var height = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);

    return height;
}

function onLinkClick(cb) {
    Array.prototype.forEach.call(document.getElementsByTagName("a"), function (linkElement) {
        linkElement.addEventListener('click', function (ev) {
            if (ev.altKey || ev.ctrlKey || ev.metaKey || ev.shiftKey || ev.defaultPrevented) {
                return true;
            }
            ev.preventDefault();
            cb(linkElement.href);
            return false;
        })
    });
}

var rpc = new easyXDM.Rpc(
    {
        onReady: function () {
            function computeAndSetHeight() {
                rpc.setHeight(computeDocumentHeight());
            }
            window.addEventListener('resize', function (ev) {
                computeAndSetHeight();
            });
            computeAndSetHeight();
            onLinkClick(function (url) {
                rpc.navigateTo(url);
            })
        }
    },
    {
        remote: {
            navigateTo: {},
            setHeight: {}
        }
    }
);

setupPage();
